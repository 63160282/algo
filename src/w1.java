import java.util.Arrays;
import java.util.Collections;

public class w1 {
	
	public static void main(String[] args) {
        
        Integer[] array = {1, 2, 3, 4, 5};
        System.out.println("Input:" + Arrays.toString(array));

        Collections.reverse(Arrays.asList(array));

        System.out.println("Output:" + Arrays.toString(array));
    }
}