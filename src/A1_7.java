import java.io.*;
import java.util.HashSet;

public class A1_7 {
	
		static void printpairs(int arr[], int sum)
		{
			HashSet<Integer> s = new HashSet<Integer>();
			for (int i = 0; i < arr.length; ++i) {
				int temp = sum - arr[i];

				if (s.contains(temp)) {
					System.out.println("Yes");
					return;
				}
				s.add(arr[i]);
			}
			System.out.println("No");
		}

		public static void main(String[] args)
		{
			int A[] = { 5, 4, 6, 6, 3, 9 };
			int n = 16;
			printpairs(A, n);
		}
	}

