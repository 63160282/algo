import java.util.*;

public class A1_10 {

		public static void printLongestIncSubArr(int arr[],int n){

			int max = 1, len = 1, maxIndex = 0;
			for (int i = 1; i < n; i++){

				if (arr[i] > arr[i-1])
					len++;
				else{
					if (max < len){
						max = len;
						maxIndex = i - max;
					}
					len = 1;
				}
			}
			if (max < len){
				max = len;
				maxIndex = n - max;
			}
			for (int i = maxIndex; i < max+maxIndex; i++)
				System.out.print(arr[i] + " ");
		}
		public static void main(String[] args) {
			int arr[] = {2, 4, 3, 1, 7, 8, 9, 1, 7};
			int n = arr.length;
			printLongestIncSubArr(arr, n);
			
		}
	}

